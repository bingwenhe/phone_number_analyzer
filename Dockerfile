FROM nickgryg/alpine-pandas:3.7.2

COPY app/requirements.txt /


RUN pip install -r /requirements.txt

COPY . /app

WORKDIR /app

CMD ["python", "app/backend.py"]

EXPOSE 5000