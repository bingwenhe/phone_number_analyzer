from collections import defaultdict

from bisect import bisect_left

import global_var
import exceptions


# Sort the data frame by the length of prefix, from longest to shortest, then match with input_phone_number
# Return values in the first row that has a matching prefix
def analyze_one_company_loop(input_phone_number, one_company_df):
    prefix_price_df = one_company_df.sort_values(by=['prefix'], ascending=False)

    prefix_price_df.reset_index(inplace=True)

    # Todo: the string operation here could be very inefficient. Will need to benchmark.
    #       Another alternative could be mod 10 of the input phone number
    # Todo: We should use binary search here for matching prefix
    prefix_price_df['match'] = prefix_price_df['prefix'].apply(lambda x: str(input_phone_number).startswith(str(x)))
    result_df = prefix_price_df[prefix_price_df.match]

    if result_df.empty:
        return None

    return {
        'prefix': result_df.iloc[0]['prefix'],
        'price': result_df.iloc[0]['price']
    }


def analyze_one_company_binary_search(input_phone_number, one_company_df):
    prefix_list = one_company_df['prefix'].tolist()
    prefix_list.sort()

    max_length = len(str(prefix_list[-1]))
    min_length = len(str(prefix_list[0]))

    correct_prefix = -1
    for i in range(max_length, min_length-1, -1):
        target = int(str(input_phone_number)[:i])

        tmp = bisect_left(prefix_list, target)

        if tmp == len(prefix_list):  # The target value is larger than ANY element in the list
            continue

        if prefix_list[tmp] == target:
            correct_prefix = target
            break

    if correct_prefix == -1:
        return None

    return {
        'prefix': correct_prefix,
        'price': one_company_df.loc[one_company_df['prefix'] == correct_prefix, 'price'].values[0]
    }


def analyze_one_company(input_phone_number, one_company_df):
    return analyze_one_company_binary_search(input_phone_number, one_company_df)
    # return analyze_one_company_loop(input_phone_number, one_company_df)


#  Find the target partition based on the first N digits of the input phone number
#  NOTE: if the first N digits are not in the partition, we need to try with N-1, and so on
#  E.g., if you have '12' and '1' as partition keys and the input phone number is 1312341235, the first two digits
#  are '13', which matches neither partitions, so we need to try with '1', which matches partition with key '1'
def find_target_partition(
        input_phone_number,
        price_list_partitions=global_var.price_list_partitions,
        first_n=global_var.FIRST_N):

    for n in range(first_n, 0, -1):
        input_prefix = str(input_phone_number)[:n]

        if input_prefix in price_list_partitions.groups:
            return price_list_partitions.get_group(input_prefix)

    raise exceptions.PrefixNotFoundGlobally()


def find_lowest_price(input_phone_number):

    target_partition = find_target_partition(input_phone_number)
    result_dict = defaultdict(list)
    for (company_name, company_df) in target_partition.groupby('company'):

        result = analyze_one_company(input_phone_number, company_df)  # Find the lowest price within a company

        if result is None:
            continue

        price = result['price']
        prefix = int(result['prefix'])

        result_dict[price].append({
            'company': company_name,
            'prefix': prefix
        })

    if not list(result_dict.keys()):
        print("!!!!This number do not match ANY company!!!!!: ", input_phone_number)
        raise exceptions.PrefixNotFoundInPartition()

    min_value = min(result_dict.keys())

    return {
        'lowest_price': min_value,
        "company_info": result_dict[min_value]
    }
