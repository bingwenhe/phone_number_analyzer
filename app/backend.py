#!/usr/bin/env python

import connexion
from flask_cors import CORS

from exceptions import PrefixNotFoundGlobally, PrefixNotFoundInPartition
from exceptions_handler import handle_prefix_not_found_globally_exception, handle_prefix_not_found_in_partition_exception

# Create the application instance
app = connexion.App(__name__, specification_dir='./')

# Read the swagger.yml file to configure the endpoints
app.add_api("swagger.yml", validate_responses=True)
app.add_error_handler(PrefixNotFoundGlobally, handle_prefix_not_found_globally_exception)
app.add_error_handler(PrefixNotFoundInPartition, handle_prefix_not_found_in_partition_exception)


CORS(app.app)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
