class PrefixNotFoundGlobally(Exception):
    status_code = 204

    def __init__(self):
        Exception.__init__(self)
        self.message = "The number you dialed is not supported by any company"

    def to_dict(self):
        return {
            'message': self.message
        }


class PrefixNotFoundInPartition(Exception):
    status_code = 204

    def __init__(self):
        Exception.__init__(self)
        self.message = "The number you dialed is not supported by any company"

    def to_dict(self):
        return {
            'message': self.message
        }