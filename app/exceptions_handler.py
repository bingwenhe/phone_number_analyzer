def handle_prefix_not_found_globally_exception(e):
    return e.message, 400


def handle_prefix_not_found_in_partition_exception(e):
    return e.message, 400