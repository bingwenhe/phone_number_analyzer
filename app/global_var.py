#!/usr/bin/env python

import os
import glob
import pandas as pd


FIRST_N = 2


# Read all the price list files into a data frame, and then group them by the first N digits of the prefix
def load_price_list_files():
    all_df_list = []
    all_files = glob.glob("app/price_list_files/*", recursive=True)
    print("Loading files ...")
    for one_file in all_files:
        with open(one_file) as f:
            company = os.path.splitext(os.path.basename(one_file))[0]
            df = pd.read_csv(f)
            df['company'] = company
            all_df_list.append(df)

    complete_df = pd.concat(all_df_list)
    result = complete_df.groupby(complete_df['prefix'].astype(str).str[:FIRST_N])
    return result


price_list_partitions = load_price_list_files()
