import random
import pandas as pd
import os

FILE_COUNT = 100
ROW_COUNT = 100


def generate_one_file(filename):
    outdir = 'generate_files'

    if not os.path.exists(outdir):
        os.mkdir(outdir)

    prefix_list = random.sample(range(1, 10000), ROW_COUNT)
    price_list = [random.randrange(50, 140) / 100 for _i in range(1, ROW_COUNT + 1)]
    df = pd.DataFrame({
        'prefix': prefix_list,
        'price': price_list
    })
    df.to_csv(os.path.join(outdir,filename))


for i in range(1, 1+FILE_COUNT):
    suffix = str(i).zfill(3)
    generate_one_file(f'company_{suffix}.csv')
