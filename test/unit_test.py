import sys
sys.path.append('app')
from app.api import analyze_one_company, find_target_partition
import pandas as pd
import global_var
import pytest
import exceptions


@pytest.mark.parametrize("input_number, prefix, price",
                         [
                             (1523412341, 1, 0.8),
                             (1232341334, 123, 0.5),
                             (1263234334, 12, 0.9),
                         ])
def test_analyze_one_company(input_number, prefix, price):
    df = pd.DataFrame({
        'prefix': [123, 23, 13333, 1, 12],
        'price': [0.5, 0.6, 0.7, 0.8, 0.9],
    })
    result = analyze_one_company(input_number, df)
    assert result['prefix'] == prefix
    assert result['price'] == price


@pytest.mark.parametrize("prefix_list,input_number,expected, exception",
                         [
                             ([12, 1], 1234, 12, None),
                             ([12, 1], 13234, 1, None),
                             ([12, 1], 513234, None, exceptions.PrefixNotFoundGlobally),
                         ])
def test_find_target_partition(prefix_list,input_number, expected, exception):
    global_var.FIRST_N = 2
    df = pd.DataFrame({
        'prefix': prefix_list,
        'value': ['a', 'b']
    })
    if exception is None:
        xx = find_target_partition(input_number, df.groupby(df['prefix'].astype(str).str[:global_var.FIRST_N]))
        assert expected == xx.iloc[0]['prefix']
    else:
        with pytest.raises(exception):
            find_target_partition(input_number, df.groupby(df['prefix'].astype(str).str[:global_var.FIRST_N]))

